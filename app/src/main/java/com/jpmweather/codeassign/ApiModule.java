package com.jpmweather.codeassign;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

public class ApiModule {

    private static final String OPEN_WEATHER_API_URL = "http://api.openweathermap.org/data/2.5/";

    private static OpenWeatherApiService ApiServiceClient;

    static {

        ApiServiceClient = provideRetrofit( provideOkhttpClient() ).create(OpenWeatherApiService.class);
    }

    public static OpenWeatherApiService getApiServiceClient() {
        return ApiServiceClient;
    }

    private static OkHttpClient provideOkhttpClient() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(25, TimeUnit.SECONDS);
        httpClient.readTimeout(25, TimeUnit.SECONDS);


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);


        return httpClient.build();
    }


    private static Retrofit provideRetrofit( OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(OPEN_WEATHER_API_URL)
                .client(okHttpClient)
                .build();
    }
}
