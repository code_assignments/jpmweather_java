
package com.jpmweather.codeassign.datamodels;

import java.util.ArrayList;


public class WeatherApiResponse {
    public WeatherApiResponse() {
        this.list = new ArrayList<>();
    }

    public ArrayList<CityWeather> getResults() {
        return list;
    }

    private ArrayList<CityWeather> list;

}
