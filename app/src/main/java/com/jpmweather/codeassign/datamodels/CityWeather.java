package com.jpmweather.codeassign.datamodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CityWeather implements Parcelable {

    public String getRawTime() {
        return rawTime;
    }

    public void setRawTime(String rawTime) {
        this.rawTime = rawTime;
    }

    public String getFormattedTime() {
        return formattedTime;
    }

    public void setFormattedTime(String formattedTime) {
        this.formattedTime = formattedTime;
    }

    public String getWeatherDescription() {
        return weather.get(0).description;
    }

    @SerializedName("dt")
    private String rawTime;

    @SerializedName("dt_txt")
    private String formattedTime;

    @SerializedName("weather")
    public ArrayList<GenWeather> weather = new ArrayList<GenWeather>();

    @SerializedName("main")
    public MainReport main;

    @SerializedName("wind")
    public WindReport wind;

     public class MainReport {
         public String getTemperature() {
             return Math.round(temperature)+"";
         }

         public void setTemperature(float temperature) {
             this.temperature = temperature;
         }

         public String getHumidity() {
             return humidity+"";
         }

         public void setHumidity(int humidity) {
             this.humidity = humidity;
         }

         public String getPressure() {
             return  Math.round(pressure)+"";
         }

         public void setPressure(float pressure) {
             this.pressure = pressure;
         }

         public float getMinTemp() {
             return minTemp;
         }

         public void setMinTemp(float minTemp) {
             this.minTemp = minTemp;
         }

         public float getMaxTemp() {
             return maxTemp;
         }

         public void setMaxTemp(float maxTemp) {
             this.maxTemp = maxTemp;
         }

         @SerializedName("temp")
        private float temperature;

        @SerializedName("humidity")
        private int humidity;

        @SerializedName("pressure")
        private float pressure;

        @SerializedName("temp_min")
        private float minTemp;

        @SerializedName("temp_max")
        private float maxTemp;
    }


    public class WindReport {

        public String getWindInfo() {
           int dir = Math.round(deg);
           String wInfo="?";
           if( dir>337 || ( dir>=0 && dir<23) ) wInfo="N";
           if( dir>=23 && dir<68) wInfo="NE";
           if( dir>=68 && dir<113) wInfo="E";
           if( dir>=113 && dir<158) wInfo="SE";
           if( dir>=158 && dir<203) wInfo="S";
           if( dir>=203 && dir<248) wInfo="SW";
           if( dir>=248 && dir<293) wInfo="W";
           if( dir>=293 && dir<=338) wInfo="NW";

           wInfo+=" "+Math.round(speed);

           return wInfo;
        }


        public float getSpeed() {
            return speed;
        }

        public void setSpeed(float speed) {
            this.speed = speed;
        }

        public float getDeg() {
            return deg;
        }

        public void setDeg(float deg) {
            this.deg = deg;
        }

        @SerializedName("speed")
        private float speed;

        @SerializedName("deg")
        private float deg;
    }

    public class GenWeather {
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        @SerializedName("id")
        private int id;

        @SerializedName("main")
        private String main;

        @SerializedName("description")
        private String description;

        @SerializedName("icon")
        public String icon;
    }


    public CityWeather() {
    }

    protected CityWeather(Parcel in) {

    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Creator<CityWeather> CREATOR = new Creator<CityWeather>() {
        @Override
        public CityWeather createFromParcel(Parcel source) {
            return new CityWeather(source);
        }

        @Override
        public CityWeather[] newArray(int size) {
            return new CityWeather[size];
        }
    };

}
