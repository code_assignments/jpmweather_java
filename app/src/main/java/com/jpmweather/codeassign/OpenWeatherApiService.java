
package com.jpmweather.codeassign;

import com.jpmweather.codeassign.datamodels.WeatherApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenWeatherApiService {

    @GET("forecast")
    Call<WeatherApiResponse> getWeatherReport(@Query("q") String cityName,
                                              @Query("mode") String mode,
                                              @Query("units") String units,
                                              @Query("appid") String ids);

}
