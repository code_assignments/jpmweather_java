
package com.jpmweather.codeassign;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class SearchScreenViewModel extends ViewModel {

    private MutableLiveData<String> cityName = new MutableLiveData<>("");
    private MutableLiveData<String> cityNameError = new MutableLiveData<>("");

    public MutableLiveData<String> getCityName() {
        return cityName;
    }
    public MutableLiveData<String> getCityNameError() {
        return cityNameError;
    }

    WeatherRepository mWeatherRepository;

    // **********************************************************


    public void setWeatherRepository( WeatherRepository weatherRepository ) {
        mWeatherRepository = weatherRepository;
    }


    // **********************************************************

    public void onSearchClicked() {

        mWeatherRepository.searchCityWeather(cityName.getValue());
    }


    // **********************************************************





}

