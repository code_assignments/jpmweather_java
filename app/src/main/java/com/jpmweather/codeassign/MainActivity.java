package com.jpmweather.codeassign;

import android.os.Bundle;

import com.jpmweather.codeassign.datamodels.CityWeather;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.test_layout);
        RecyclerView recv = findViewById(R.id.recv1);
        WeatherDetailsListAdapter wdAdapter = new WeatherDetailsListAdapter();
        recv.setAdapter(wdAdapter);
        recv.setHasFixedSize(true);
        recv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        WeatherRepository weatherRepository = new WeatherRepository();
        weatherRepository.searchCityWeather("San Francisco");
        weatherRepository.getWeather().observe(this, new Observer<ArrayList<CityWeather>>() {
            @Override
            public void onChanged(ArrayList<CityWeather> cityWeathers) {
                    wdAdapter.setItems(cityWeathers);
            }
        });


    }



}
