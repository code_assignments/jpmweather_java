package com.jpmweather.codeassign;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.jpmweather.codeassign.datamodels.CityWeather;
import com.jpmweather.codeassign.datamodels.WeatherApiResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherRepository {

    private MutableLiveData<ArrayList<CityWeather>> retv = new MutableLiveData<>();

    public LiveData<ArrayList<CityWeather>> getWeather() {
        return retv;
    }


    public void searchCityWeather(String cityName) {

        Call<WeatherApiResponse> call = ApiModule.getApiServiceClient().getWeatherReport( cityName, "json", "imperial", "64f7dbd5a9b577f25d17aa922f4f16ab");

        call.enqueue(new Callback<WeatherApiResponse>()
        {
            @Override
            public void onResponse(Call<WeatherApiResponse> call, Response<WeatherApiResponse> response) {
                 retv.setValue(response.body().getResults());
            }

            @Override
            public void onFailure(Call<WeatherApiResponse> call, Throwable t) {
                 retv.setValue( null );

            }


        });


    }


/*
    public void searchPeople(String keyword) {

        Call<PeopleApiResponse> call = ApiModule.getApiServiceClient().searchPeople(keyword);

        call.enqueue(new Callback<PeopleApiResponse>()
        {
            @Override
            public void onResponse(Call<PeopleApiResponse> call, Response<PeopleApiResponse> response) {
                retv.setValue(response.body().getResults().get(0));
            }

            @Override
            public void onFailure(Call<PeopleApiResponse> call, Throwable t) {
                retv.setValue( new People());
            }


        });


    }

    */
}
