package com.jpmweather.codeassign;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.jpmweather.codeassign.databinding.WeatherDetailsListItemBinding;
import com.jpmweather.codeassign.datamodels.CityWeather;

import java.util.ArrayList;


public class WeatherDetailsListAdapter extends RecyclerView.Adapter<WeatherDetailsListAdapter.CustomViewHolder> {

        private ArrayList<CityWeather> cityWeatherList;
        public WeatherDetailsListAdapter( ) {
            cityWeatherList=new ArrayList<CityWeather>();
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            WeatherDetailsListItemBinding itemBinding = WeatherDetailsListItemBinding.inflate(layoutInflater, parent, false);
            CustomViewHolder viewHolder = new CustomViewHolder(itemBinding);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(CustomViewHolder holder, int position) {
            CityWeather cityWeather = cityWeatherList.get(position);

            holder.binding.temperatureBox.setText( cityWeather.main.getTemperature() );
            holder.binding.weatherBox.setText( cityWeather.getWeatherDescription() );
            holder.binding.dateBox.setText( cityWeather.getFormattedTime());
            holder.binding.humidityBox.setText( "Humidity: "+cityWeather.main.getHumidity() );
            holder.binding.pressureBox.setText( "Presuure: "+cityWeather.main.getPressure());
            holder.binding.windBox.setText( "Wind: "+cityWeather.wind.getWindInfo() );


        }

        @Override
        public int getItemCount() {
            if( cityWeatherList==null ) return 0;
            else return cityWeatherList.size();
        }


        public void setItems(ArrayList<CityWeather>  cityWeatherList) {
        this.cityWeatherList=cityWeatherList;
        notifyDataSetChanged();
        }

        /* ******************************************************* */

        protected class CustomViewHolder extends RecyclerView.ViewHolder {
            private WeatherDetailsListItemBinding binding;

            public CustomViewHolder(WeatherDetailsListItemBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }


